package model;

public class Tokimon {

    private int id;
    private String name;
    private double weight;
    private double height;
    private double strength;
    private String ability;
    private String color;


    public Tokimon() {
        this.id = 0;
        this.name = "-";
        this.weight = 0.0;
        this.height = 0.0;
        this.strength = 0.0;
        this.ability = "-";
        this.color = "-";
    }

    public Tokimon(int id, String name, double weight, double height, double size, String ability, String color) {
        this.id = id;
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.strength = size;
        this.ability = ability;
        this.color = color;
    }

    @Override
    public String toString() {
        return "Tokimon {" +
                "id=" + id + '\'' +
                "name='" + name + '\'' +
                "weight='" + weight + '\'' +
                "height='" + height + '\'' +
                ", strength=" + strength +
                ", ability='" + ability + '\'' +
                ", color=" + color +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double size) {
        this.strength = size;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
