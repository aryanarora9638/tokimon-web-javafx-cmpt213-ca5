package ca.cmpt213.asn5.client;

import java.io.BufferedReader;
import java.io.IOException;

import com.google.gson.Gson;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * The following class creates the UI using JAVAFx and requests / post any updates to the server.
 * @author Aryan Arora
 */
public class Client extends Application {
    private List<Tokimon> tokimons = new ArrayList<>();
    private Map<Tokimon, Button> tokiButton = new HashMap<>();


    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) throws Exception {
        requestTokimonsFromServer();
        System.out.println("Initial data received");
        for(Tokimon tokimon : this.tokimons){
            System.out.println(tokimon.getName());
        }

        GridPane gridPane = new GridPane();

        createSearchBar(gridPane);

        Button addTokimonBtn = new Button("Add New Tokimon");
        addTokimonBtn.setOnAction(actionEvent ->showAddNewTokimonPage(gridPane));

        for(Tokimon tokimon : tokimons){
            generateTokimonButtons(tokimon,gridPane);
        }

        gridPane.add(addTokimonBtn,0,this.tokimons.size()+1);
        Scene scene = new Scene(gridPane);
        primaryStage.setTitle("JavaFX Demo");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void createSearchBar(GridPane gridPane) {
        HBox searchBox = new HBox(7);
        TextField searchTextField = new TextField();
        Label searchToki = new Label("Search Toki By ID: ");
        Button findTokiNow = new Button("Find");
        findTokiNow.setOnAction(actionEvent -> {
            if(!searchTextField.getText().isEmpty()){
                String tokiID = searchTextField.getText().trim();
                Tokimon requestedToki = new Tokimon();
                try {
                    Gson gson = new Gson();
                    URL url = new URL("http://localhost:8080/api/tokimon/" + tokiID);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setRequestMethod("GET");
                    connection.connect();

                    System.out.println("error trying" + connection.getResponseCode());
                    if(connection.getResponseCode() != 200){
                        requestedToki = new Tokimon();
                        Alert fail= new Alert(Alert.AlertType.INFORMATION);
                        fail.setHeaderText("Toki Not Found");
                        fail.setContentText("Requested Toki Does not Exist");
                        fail.showAndWait();
                    }
                    else {
                        BufferedReader bufferedReader = new BufferedReader(
                                new InputStreamReader(connection.getInputStream()));
                        System.out.println(connection.getResponseCode());
                        String output;
                        StringBuilder stringBuilder = new StringBuilder();
                        while ((output = bufferedReader.readLine()) != null){
                            stringBuilder.append(output);
                            System.out.println(output);
                        }
                        requestedToki = gson.fromJson(stringBuilder.toString(),Tokimon.class);
                    }

                    connection.disconnect();

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                individualTokiPage(requestedToki, gridPane);
            }
            else {
                Alert fail= new Alert(Alert.AlertType.INFORMATION);
                fail.setHeaderText("Missing Fields");
                fail.setContentText("Please enter a valid ID");
                fail.showAndWait();
            }

        });

        searchBox.getChildren().addAll(searchToki,searchTextField, findTokiNow);
        searchBox.setAlignment(Pos.CENTER);
        gridPane.add(searchBox,0,0);
    }

    private void generateTokimonButtons(Tokimon tokimon,GridPane gridPane) {
            Button tokiButton = new Button();

            StackPane stackPane = new StackPane();
            //toki image
            String imageName = getImageType(tokimon.getAbilityMap());
            imageName = "imageFiles/"+imageName+".jpg";
            System.out.println(tokimon.getId());
            ImageView tokiImageView = new ImageView(new Image(imageName));
            tokiImageView.setFitWidth(adjustValue(tokimon.getWeight()));
            tokiImageView.setFitHeight(adjustValue(tokimon.getHeight()));

            //toki name
            Text tokiName = new Text(tokimon.getName());
            tokiName.setTextAlignment(TextAlignment.LEFT);
            tokiName.setUnderline(true);
            tokiName.setFont(Font.font("Verdana", FontWeight.BOLD, 10));

            stackPane.getChildren().addAll(tokiImageView,tokiName);
            tokiButton.setGraphic(stackPane);

            this.tokiButton.put(tokimon, tokiButton);

            gridPane.add(tokiButton,tokimon.getId(),1);
            tokiButton.setOnAction(actionEvent -> {
                System.out.println("clicked button number" + tokimon.getName());
                //PAGE FOR EACH TOKIMON INITIATES HERE
                individualTokiPage(tokimon, gridPane);
            });

    }

    private double adjustValue(double height) {
        double val = height;
        if(val < 70){
            while (val < 70){
                val+=30;
            }
        }
        return val;
    }

    private void individualTokiPage(Tokimon tokimon, GridPane gridPane) {
        ObservableList<Tokimon> tokimons = FXCollections.observableArrayList(tokimon);
        Stage tokiPage = new Stage();
        TableView tableView = new TableView();

        Scene scene = new Scene(new Group());
        tokiPage.setTitle("Toki Info.");
        tokiPage.setHeight(300);

        final Label label = new Label("Tokimon Information");
        label.setFont(new Font("Arial", 20));

        tableView.setEditable(false);

        TableColumn idCol = new TableColumn("ID");
        TableColumn nameCol = new TableColumn("Name");
        TableColumn weightCol = new TableColumn("WEIGHT");
        TableColumn heightCol = new TableColumn("HEIGHT");
        TableColumn strengthCol = new TableColumn("STRENGTH");
        TableColumn maxAbility = new TableColumn("MAX ABILITY");
        maxAbility.setMinWidth(100);
        TableColumn waterCol = new TableColumn("WATER");
        TableColumn flyCol = new TableColumn("FLY");
        TableColumn fireCol = new TableColumn("FIRE");
        TableColumn electricCol = new TableColumn("ELECTRIC");
        TableColumn freezeCol = new TableColumn("FREEZE");
        TableColumn colorCol = new TableColumn("COLOR");

        idCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("id"));
        nameCol.setCellValueFactory( new PropertyValueFactory<Tokimon,String>("name"));
        weightCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("weight"));
        heightCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("height"));
        strengthCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("strength"));
        maxAbility.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("MaxAbility"));
        waterCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("Water"));
        flyCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("Fly"));
        fireCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("Fire"));
        electricCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("Electric"));
        freezeCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("Freeze"));
        colorCol.setCellValueFactory(new PropertyValueFactory<Tokimon,String>("Color"));


        tableView.getColumns().addAll(idCol,nameCol,weightCol,heightCol,strengthCol,maxAbility,waterCol,flyCol,fireCol,electricCol,freezeCol,colorCol);
        tableView.setItems(tokimons);

        Button deleteToki = new Button("Delete Toki");
        deleteToki.setAlignment(Pos.CENTER);
        deleteToki.setOnAction(actionEvent -> {

            try {
                URL url = new URL("http://localhost:8080/api/tokimon/" + tokimon.getId());
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("DELETE");
                connection.connect();
                if(connection.getResponseCode() == 200){
                    System.out.println("before size ->" + this.tokimons.size());
                    requestTokimonsFromServer();
                    System.out.println("after size ->" + this.tokimons.size());
                    Alert fail = new Alert(Alert.AlertType.INFORMATION);
                    fail.setHeaderText("Toki Deleting");
                    fail.setContentText("Toki is being deleted");
                    fail.showAndWait();
                    tokiPage.close();

                    System.out.println("removing ur toki");
                    System.out.println(gridPane.getChildren().size());
                    System.out.println(gridPane.getChildren().contains(this.tokiButton.get(tokimon)));
                    gridPane.getChildren().remove(this.tokiButton.get(tokimon));
                    this.tokiButton.remove(tokimon);
                    System.out.println(gridPane.getChildren().size());
                    return;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        });

        final VBox vbox = new VBox(5);
        final HBox hBox = new HBox(5);
        hBox.getChildren().addAll(label, deleteToki);
        vbox.setSpacing(5);
        vbox.setPadding(new Insets(10, 0, 0, 10));
        vbox.getChildren().addAll(hBox, tableView);


        ((Group) scene.getRoot()).getChildren().addAll(vbox);
        tokiPage.setScene(scene);
        tokiPage.show();
    }

    private String getImageType(Map<String, Integer> abilityMap) {
        String type="";
        Integer maxAbilityScore = 0;
        Integer temp = 0;

        for(String ability : abilityMap.keySet()){
            temp = abilityMap.get(ability);
            if(temp >= maxAbilityScore){
                maxAbilityScore = temp;
                type = ability;
            }
        }
        System.out.println("type is - " + type);
        return type;
    }

    private void showAddNewTokimonPage(GridPane gridPane) {
        final int spacing = 5;
        Stage newTokimon = new Stage();
        VBox vBox = new VBox(spacing);
        HBox hBox1 = new HBox(spacing);
        HBox hBox2 = new HBox(spacing);
        HBox hBox3 = new HBox(spacing);
        HBox hBox4 = new HBox(spacing);
        HBox hBox5 = new HBox(spacing);
        HBox hBox6 = new HBox(spacing);


        Label nameLabel = new Label("Name:      ");
        Label weightLabel = new Label("Weight:    ");
        Label heightLabel = new Label("Height:     ");
        Label strengthLabel = new Label("Strength:  ");
        Label abilityLabel = new Label("Abilities:      ");
        Label colorLabel = new Label("Color:       ");

        List<Label> labels = new ArrayList<>();
        labels.add(nameLabel);
        labels.add(weightLabel);
        labels.add(heightLabel);
        labels.add(strengthLabel);
        labels.add(colorLabel);



        TextField nameTextField = new TextField();
        nameTextField.setPromptText("Cannot be a number");
        TextField weightTextField = new TextField();
        weightTextField.setPromptText("Suggested >= 70.0");
        TextField heightTextField = new TextField();
        heightTextField.setPromptText("Suggested >= 70.0");
        TextField strengthTextField = new TextField();
        //TextField abilityTextField = new TextField();
        TextField colorTextField = new TextField();
        colorTextField.setPromptText("Cannot be a number");

        List<TextField> textFields = new ArrayList<>();
        textFields.add(nameTextField);
        textFields.add(weightTextField);
        textFields.add(heightTextField);
        textFields.add(strengthTextField);
        //textFields.add(abilityTextField);
        textFields.add(colorTextField);

        VBox abilitiesVbox = new VBox(spacing);
        List<TextField> abilitiesTextField = new ArrayList<>();
        List<Label> abilitiesLabel = new ArrayList<>();

        abilitiesTextField.add(new TextField());
        abilitiesTextField.add(new TextField());
        abilitiesTextField.add(new TextField());
        abilitiesTextField.add(new TextField());
        abilitiesTextField.add(new TextField());

        abilitiesLabel.add(new Label("Fly        "));
        abilitiesLabel.add(new Label("Fire       "));
        abilitiesLabel.add(new Label("Water   "));
        abilitiesLabel.add(new Label("Freeze  "));
        abilitiesLabel.add(new Label("Electric "));

        for(TextField textField : abilitiesTextField){
            textField.setPromptText("Value b/w 0 and 100");
        }

        HBox abilities1 = new HBox();
        abilities1.getChildren().addAll(abilitiesLabel.get(0), abilitiesTextField.get(0));
        HBox abilities2 = new HBox();
        abilities2.getChildren().addAll(abilitiesLabel.get(1), abilitiesTextField.get(1));
        HBox abilities3 = new HBox();
        abilities3.getChildren().addAll(abilitiesLabel.get(2), abilitiesTextField.get(2));
        HBox abilities4 = new HBox();
        abilities4.getChildren().addAll(abilitiesLabel.get(3), abilitiesTextField.get(3));
        HBox abilities5 = new HBox();
        abilities5.getChildren().addAll(abilitiesLabel.get(4), abilitiesTextField.get(4));

        abilitiesVbox.getChildren().addAll(abilities1,abilities2,abilities3,abilities4,abilities5);



        Button confirmTokimon = new Button("Confirm");
        confirmTokimon.setOnAction(actionEvent -> {
            boolean allCompleted = true;
            for(TextField textField : textFields) {
                if (textField.getText().trim().isEmpty()) {
                    Alert fail = new Alert(Alert.AlertType.INFORMATION);
                    fail.setHeaderText("Missing Fields");
                    fail.setContentText("All Fields are Mandatory");
                    fail.showAndWait();
                    allCompleted = false;
                    return;
                }
            }
            if(checkNaN(textFields.get(1)) || checkNaN(textFields.get(2)) || checkNaN(textFields.get(3))){
                Alert fail = new Alert(Alert.AlertType.INFORMATION);
                fail.setHeaderText("Invalid Fields");
                fail.setContentText("Field value unsupported - NaN(not a number)");
                fail.showAndWait();
                return;
            }

            if(!checkNaN(textFields.get(0)) || !checkNaN(textFields.get(4))){
                Alert fail = new Alert(Alert.AlertType.INFORMATION);
                fail.setHeaderText("Invalid Fields");
                fail.setContentText("Field value unsupported - String(value expected)");
                fail.showAndWait();
                return;
            }
            if(allCompleted && fieldsValidated(abilitiesTextField)){
                    Map<String, Integer> abilitiesHashMap = new HashMap<>();
                    for (int i = 0; i < abilitiesTextField.size(); i++){
                        abilitiesHashMap.put(abilitiesLabel.get(i).getText().trim(), Integer.parseInt(String.valueOf(abilitiesTextField.get(i).getText().trim())));
                    }
                    Tokimon addTokimon = new Tokimon(nameTextField.getText().trim(), Double.valueOf(weightTextField.getText().trim()), Double.valueOf(heightTextField.getText().trim()),
                            Double.valueOf(strengthTextField.getText().trim()), "ability-remove-it-later", colorTextField.getText().trim(), abilitiesHashMap);

                    System.out.println("add tokimon - " + addTokimon.getName());
                    addTokimon.setId(this.tokimons.size());
                    if(this.tokimons.contains(addTokimon)){
                        Alert fail = new Alert(Alert.AlertType.INFORMATION);
                        fail.setHeaderText("Tokimon Exists");
                        fail.setContentText("The tokimon you added already exists");
                        fail.showAndWait();
                        return;
                    }
                    postTokimonToServer(addTokimon);
                    requestTokimonsFromServer();
                    System.out.println("size -> " + this.tokimons.size());

                    generateTokimonButtons(this.tokimons.get(this.tokimons.size()-1),gridPane);
                    newTokimon.close();
                    return;
            }
        });


        hBox1.getChildren().addAll(nameLabel,nameTextField);
        hBox2.getChildren().addAll(weightLabel,weightTextField);
        hBox3.getChildren().addAll(heightLabel,heightTextField);
        hBox4.getChildren().addAll(strengthLabel,strengthTextField);
        hBox5.getChildren().addAll(abilityLabel,abilitiesVbox);
        hBox6.getChildren().addAll(colorLabel,colorTextField);


        vBox.getChildren().addAll(hBox1,hBox2,hBox3,hBox4,hBox5,hBox6,confirmTokimon);
        vBox.setAlignment(Pos.CENTER);
        Scene scene = new Scene(vBox);
        newTokimon.setScene(scene);
        newTokimon.show();
    }

    private boolean fieldsValidated(List<TextField> abilitiesTextField) {
        for(TextField textField : abilitiesTextField){
            if(checkNaN(textField)){
                Alert fail = new Alert(Alert.AlertType.INFORMATION);
                fail.setHeaderText("Invalid Fields");
                fail.setContentText("Field value unsupported - NaN(not a number)");
                fail.showAndWait();
                return false;
            }
            int value = Integer.parseInt(textField.getText().trim());
            if(value<0 || value>100){
                Alert fail = new Alert(Alert.AlertType.INFORMATION);
                fail.setHeaderText("Invalid Fields");
                fail.setContentText("Abilities Field value unsupported - Should be between 0 and 100(inclusive)");
                fail.showAndWait();
                return false;
            }
        }
        return true;
    }

    private boolean checkNaN(TextField textField) {
        try{
            Integer.parseInt(textField.getText().trim());
            return false;
        } catch(NumberFormatException e){
            return true;
        }
    }

    private void postTokimonToServer(Tokimon addTokimon) {
        try {
            Gson gson = new Gson();
            URL url = new URL("http://localhost:8080/api/tokimon/add");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/json");

            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());

            String output = gson.toJson(addTokimon);
            System.out.println("Posting output data - " + output);
            outputStreamWriter.write(output);
            outputStreamWriter.flush();
            outputStreamWriter.close();

            connection.connect();
            System.out.println(connection.getResponseCode());
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestTokimonsFromServer() {
        try {
            Tokimon[] updatedTokimons;
            Gson gson = new Gson();
            URL url = new URL("http://localhost:8080/api/tokimon/all");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            System.out.println(connection.getResponseCode());
            String output;
            StringBuilder stringBuilder = new StringBuilder();
            while ((output = bufferedReader.readLine()) != null){
                stringBuilder.append(output);
            }
            updatedTokimons = gson.fromJson(stringBuilder.toString(),Tokimon[].class);
            this.tokimons = Arrays.asList(updatedTokimons);
            for(Tokimon tokimon : this.tokimons){
                System.out.println("IDS ARE - " + tokimon.getId());
            }
            connection.disconnect();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
