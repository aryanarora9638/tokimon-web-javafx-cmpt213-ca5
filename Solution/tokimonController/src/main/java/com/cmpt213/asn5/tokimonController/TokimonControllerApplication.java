package com.cmpt213.asn5.tokimonController;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The following class calls the tokimonController to set up and start the server.
 *	@author Aryan Arora
 */

@SpringBootApplication
public class TokimonControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(TokimonControllerApplication.class, args);
	}

}
