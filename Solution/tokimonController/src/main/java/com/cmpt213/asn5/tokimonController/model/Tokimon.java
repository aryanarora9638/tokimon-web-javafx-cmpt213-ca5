package com.cmpt213.asn5.tokimonController.model;

import java.util.HashMap;
import java.util.Map;

/**
 * The following class creates a tokimon and is described in terms of various attributes like name, weight, height, color, size, ability and strength.
 * @author Aryan Arora
 */

public class Tokimon {
    private int id;
    private String name;
    private double weight;
    private double height;
    private double strength;
    private Map<String, Integer> abilityMap;
    private String ability;
    private String color;


    public Tokimon() {
        this.id = 0;
        this.name = "-";
        this.weight = 0.0;
        this.height = 0.0;
        this.strength = 0.0;
        this.ability = "-";
        this.color = "-";

        String[] abilities = {"Fly", "Fire", "Water", "Freeze", "Electric"};

        this.abilityMap = new HashMap<>();
        for(String test : abilities){
            abilityMap.put(test, 0);
        }
    }

    public Tokimon(String name, double weight, double height, double strength, String ability, String color, Map<String, Integer> abilityMap ) {
        this.name = name;
        this.weight = weight;
        this.height = height;
        this.strength = strength;
        this.ability = ability;
        this.color = color;

        this.abilityMap = abilityMap;
    }

    @Override
    public String toString() {
        return "Tokimon {" +
                "id=" + id + '\'' +
                "name='" + name + '\'' +
                "weight='" + weight + '\'' +
                "height='" + height + '\'' +
                ", strength=" + strength +
                ", ability='" + ability + '\'' +
                ", color=" + color +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double size) {
        this.strength = size;
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Map<String, Integer> getAbilityMap() {
        return abilityMap;
    }

    public int getWater(){
        return this.abilityMap.get("Water");
    }

    public int getFire(){
        return this.abilityMap.get("Fire");
    }

    public int getFly(){
        return this.abilityMap.get("Fly");
    }

    public int getFreeze(){
        return this.abilityMap.get("Freeze");
    }

    public int getElectric(){
        return this.abilityMap.get("Electric");
    }

    public void setAbilityMap(Map<String, Integer> abilityMap) {
        this.abilityMap = abilityMap;
    }

    public String getMaxAbility(){
        String type="";
        Integer maxAbilityScore = 0;
        Integer temp = 0;

        for(String ability : this.abilityMap.keySet()){
            temp = this.abilityMap.get(ability);
            if(temp >= maxAbilityScore){
                maxAbilityScore = temp;
                type = ability;
            }
        }
        if(maxAbilityScore == 0){
            type = "All Equal";
        }
        return type;
    }
}

