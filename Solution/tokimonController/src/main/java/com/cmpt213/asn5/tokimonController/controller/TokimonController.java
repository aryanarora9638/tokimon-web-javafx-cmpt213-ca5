package com.cmpt213.asn5.tokimonController.controller;

import com.cmpt213.asn5.tokimonController.model.Tokimon;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * The following class setup the server and reads/writes the json file with any new updates.
 * @author Aryan Arora
 */

@RestController
public class TokimonController {
    private final AtomicLong counter = new AtomicLong(0);
    private List<Tokimon> serverTokimons = new ArrayList<>();
    private Tokimon[] jsonFileTokimons;

    @GetMapping("/api/tokimon/all")
    public List<Tokimon> getTokimon(){
        checkServer();
        System.out.println("all tokimon showing");
        return serverTokimons;
    }


    @GetMapping("/api/tokimon/{id}")
    public Tokimon getTokimonById(@PathVariable int id){
        checkServer();
        System.out.println("tokimon showing by id");
        for(Tokimon tokimon : serverTokimons){
            if(tokimon.getId() == id){
                return tokimon;
            }
        }
        //returns an empty tokimon
        throw new IllegalArgumentException("The ID requested in the path variable does not exist!!");
        //return new Tokimon();
    }

    @GetMapping("/api/tokimon/test/{id}")
    public String testString(@PathVariable int id){
        checkServer();
        Tokimon deletedTokimon = null;
        for(Tokimon tokimon : serverTokimons){
            System.out.println("searching" + tokimon.toString());
            if(tokimon.getId() == id){
                deletedTokimon = tokimon;
            }
        }
        if(serverTokimons.remove(deletedTokimon)){
            jsonFileTokimons = serverTokimons.toArray(new Tokimon[serverTokimons.size()]);
            System.out.println("Tokimon deleted - " + deletedTokimon.toString());
        }
        else {
            System.out.println("nothing found");
        }

        Gson gson = new Gson();
        String updateJsonFile = gson.toJson(this.jsonFileTokimons);
        writeJson();
        return updateJsonFile;
    }

    @PostMapping("/api/tokimon/add")
    public void addTokimon(@RequestBody Tokimon newTokimon){
        System.out.println("new tokimon post request made");
        System.out.println(newTokimon);
        newTokimon.setId((int) counter.incrementAndGet());
        serverTokimons.add(newTokimon);
        jsonFileTokimons = serverTokimons.toArray(new Tokimon[serverTokimons.size()]);
        System.out.println("New tokimon added - " + newTokimon.toString());
        writeJson();
    }

    @DeleteMapping("/api/tokimon/{id}")
    public void deleteTokimonById(@PathVariable long id){
        checkServer();
        Tokimon deletedTokimon = null;
        for(Tokimon tokimon : serverTokimons){
            System.out.println("searching" + tokimon.toString());
            if(tokimon.getId() == id){
                deletedTokimon = tokimon;
            }
        }
        if(deletedTokimon == null){
            throw new IllegalArgumentException("The ID requested in the path variable does not exist!!");
        }
        if(serverTokimons.remove(deletedTokimon)){
            jsonFileTokimons = serverTokimons.toArray(new Tokimon[serverTokimons.size()]);
        }
        else {
            System.out.println("nothing found");
        }
        writeJson();
    }


    @PostConstruct
    public void init(){
        System.out.println("Running on Port 8080");

        Gson gson = new Gson();
        StringBuilder  stringBuilder = new StringBuilder();
        String line;

        File dataDirectory = new File("data");
        File[] listFiles = dataDirectory.listFiles();

        for(File item : listFiles){
            if(item.getName().equalsIgnoreCase("tokimon.json")){
                try {
                    BufferedReader bufferedReader = new BufferedReader(new FileReader(item));
                    while ((line = bufferedReader.readLine()) != null){
                        stringBuilder.append(line);
                    }
                    this.jsonFileTokimons = (gson.fromJson(stringBuilder.toString(), Tokimon[].class));
                    System.out.println("size is -> " + jsonFileTokimons.length);
                    for(Tokimon tokimon : jsonFileTokimons){
                        System.out.println(tokimon);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void writeJson(){
        Gson gson = new Gson();
        String updateJsonFile = gson.toJson(this.jsonFileTokimons);
        System.out.println("Updated Json file " + updateJsonFile);

        try {
            FileWriter fileWriter = new FileWriter("data/tokimon.json");
            fileWriter.write(updateJsonFile);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void checkServer(){
        for(int i = 0; i < this.jsonFileTokimons.length; i++){
            if(serverTokimons.contains(this.jsonFileTokimons[i])){
               continue;
            }
            serverTokimons.add(this.jsonFileTokimons[i]);
        }
    }

}